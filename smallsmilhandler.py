#!/usr/bin/python3
# -*- coding: utf-8 -*-



from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        #inicializamos variables
        self.list = []
        self.att1 = ""
        self.att2 = ""
        self.att3 = ""
        self.att4 = ""
        self.att5 = ""

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'root-layout':
            #tomamos los valores de los atributos
            self.att1 = attrs.get('width', "")
            self.att2 = attrs.get('height', "")
            self.att3 = attrs.get('background-color', "")
            self.list.append({'etiqueta': name,'width':self.att1, 'height' : self.att2,'background-color': self.att3})

        elif name == 'region':
            self.att1 = attrs.get('id', "")
            self.att2 = attrs.get('top', "")
            self.att3 = attrs.get('bottom', "")
            self.att4 = attrs.get('left', "")
            self.att5 = attrs.get('right', "")
            self.list.append({'etiqueta': name, 'id': self.att1, 'top': self.att2, ' bottom': self.att3, 'left': self.att4, 'right': self.att5})

        elif name == 'img':
            self.att1 = attrs.get('src', "")
            self.att2 = attrs.get('region', "")
            self.att3 = attrs.get('begin', "")
            self.att4 = attrs.get('dur', "")
            self.list.append({'etiqueta': name, 'src':self.att1, 'region': self.att2,'begin': self.att3, 'dur': self.att4})

        elif name == 'audio':
            self.att1 = attrs.get('src', "")
            self.att2 = attrs.get('begin', "")
            self.att3 = attrs.get('dur', "")
            self.list.append({'etiqueta': name, 'src': self.att1, 'begin': self.att2, 'dur': self.att3})

        elif name == 'textstream':
            self.att1 = attrs.get('src', "")
            self.att2 = attrs.get('region', "")
            self.list.append({'etiqueta': name, 'src': self.att1, 'region': self.att2})

    def get_tags(self):
        return self.list


if __name__ == "__main__":

    parser = make_parser() #lee linea a linea y si detecta llama al
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open('karaoke.smil'))
    etiquetas = sHandler.get_tags()
    print(etiquetas)
