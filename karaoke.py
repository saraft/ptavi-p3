#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
import json
import sys
import urllib.request


class KaraokeLocal(SmallSMILHandler):

    def __init__(self, fichero):
        try:
            parser = make_parser()
            sHandler = SmallSMILHandler()
            parser.setContentHandler(sHandler)
            parser.parse(open(fichero))
            self.etiq = sHandler.get_tags()
        except:
            print('no existe')

    def __str__(self):
        for att in self.etiq:
            for etiq in att:
                if att[etiq] != '':
                    print(etiq, '=', att[etiq], end='\t')
                print(end='')
                print('\n')
        return(att[etiq])

    def to_json(self, fichero):
        fichjson = (fichero).replace('.smil', '.json')
        with open(fichjson, 'w') as file:
            json.dump(self.etiq, file)

    def do_local(self):
        for att in self.etiq:
            for etiq in att:
                if etiq == 'src':
                    url = att[etiq]
                    if url.startswith('http://'):
                        urls = url.split('/')[-1]
                        urllib.request.urlretrieve(url, urls)
                        print('descargado')


if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 karaoke.py file.smil")

    fichero = sys.argv[1]
    karaoke = KaraokeLocal(fichero)
    print(karaoke)
    karaoke.to_json(fichero)
    karaoke.do_local()
    karaoke.to_json('local.json')
